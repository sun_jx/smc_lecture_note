import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation
from random import gauss

# For data logging
import pandas as pd
import time

import progressbar as pb

Te = 20
ts = 2e-2

sqrt = np.sqrt
sin = np.sin
cos = np.cos
sign = np.sign
dot = np.dot
zeros = np.zeros
pinv = np.linalg.pinv
cross = np.cross
array = np.array

K_POS = array([0.95, 0.95, 1])
K_VEL_P = array([0.09, 0.09, 0.2])
MAXI_VEL = array([8, 8, 1])
MAXI_ACC = array([5, 5, 3])


def constrain_ref(x, cons):
    # Make sure cons is a positive value
    num = np.size(x)
    if (num > 1):
        for n in range(0, num):
            if (cons[n] < 0):
                cons[n] = -cons[n]
            if (x[n] > cons[n]):
                x[n] = cons[n]
            elif (x[n] < -cons[n]):
                x[n] = -cons[n]
    else:
        if (x > cons):
            x = cons
        elif (x < -cons):
            x = -cons
    return x


def constrain_saturation(x, min_value, max_value):
    # Make sure cons is a positive value
    num = np.size(x)
    if (num > 1):
        for n in range(0, num):
            if (x[n] > max_value):
                x[n] = max_value
            elif (x[n] < min_value):
                x[n] = min_value
    else:
        if (x > max_value):
            x = max_value
        elif (x < min_value):
            x = min_value
    return x


def vector3_normalize(x):
    len_x = np.linalg.norm(x)
    if len_x == 0:
        return x
    return x / len_x


def position_control(pos_sp, pos, vel):
    pos_sp = array(pos_sp)
    pos = array(pos)
    vel = array(vel)
    pos_err = pos_sp - pos
    vel_sp = K_POS * pos_err
    vel_sp = array([8.0, 0, 0])
    vel_sp = constrain_ref(vel_sp, MAXI_VEL)
    vel_err = vel_sp - vel
    acc_sp = K_VEL_P * vel_err
    acc_sp = constrain_ref(acc_sp, MAXI_ACC)
    acc_sp = acc_sp - array([0, 0, 0.5])
    thrust_max_NE_tilt = abs(acc_sp[2] * np.tan(45 / 57.3))
    lim_thr_max = 1.2
    thrust_max_NE = sqrt(lim_thr_max * lim_thr_max - acc_sp[2] * acc_sp[2])
    thr_max = thrust_max_NE_tilt
    if thrust_max_NE < thrust_max_NE_tilt:
        thr_max = thrust_max_NE

    thrust_sp_xy = acc_sp[0:2]
    thrust_sp_xy_norm = np.linalg.norm(thrust_sp_xy)

    if thrust_sp_xy_norm > thr_max:
        acc_sp[0] = acc_sp[0] / thrust_sp_xy_norm * thr_max
        acc_sp[1] = acc_sp[1] / thrust_sp_xy_norm * thr_max

    ac = np.linalg.norm(acc_sp)
    if ac < 0:
        ac = 0

    elif ac > 1:
        ac = 1
    ac = 2 * ac  # remap ac range to [0,2]
    T_c = -ac * 9.8 * mass
    acc_sp = vector3_normalize(acc_sp)
    return T_c, acc_sp


pos_smdo_state = zeros(6)
att_smdo_state = zeros(6)
Kc_P = array([0.5, 0.5, 1])
K_SMC_POS = np.array([2, 2, 4])
L_pos = np.array([0.6, 0.6, 4])

lamda0_pos = 1.5 * sqrt(L_pos)
lamda1_pos = 1.1 * L_pos
last_v_sp = 0


def smdo_aux_pos_dynamic(t, x, u, s, omega, phi0):
    z_dot = -phi0 + u - omega
    eta_dot = lamda1_pos * sign(s)
    dxdt = np.hstack((z_dot, eta_dot))
    return dxdt


def smdo_aux_att_dynamic(t, x, u, s, omega_att, phi0):
    Mu = Gu.dot(u)
    z_dot = -phi0 + Mu[0:3] - omega_att
    eta_dot = lamda1 * sign(s)
    # eta_dot = (-att_smdo_state[3:] + lamda1*0.05 *sign(s))/0.05
    for i in range(0, 3):
        if abs(s[i]) < 0.5:
            eta_dot[i] = 0.15 * lamda1[i] * sign(s[i])
            # eta_dot[i]=0;
        if abs(s[i]) < 0.01:
            # eta_dot[i] = 0.1*lamda1[i] * (s[i]/(abs(s[i])+0.0001));
            eta_dot[i] = 0

    dxdt = np.hstack((z_dot, eta_dot))
    return dxdt


def smc_smdo_position_control(pos_sp, pos, vel):
    global pos_smdo_state
    global last_v_sp
    global Log_s_pos
    global Log_sigma_pos
    global Log_V_sp
    pos_sp = array(pos_sp)
    pos = array(pos)
    vel = array(vel)
    pos_err = pos_sp - pos

    # velocity loop:
    vel_sp = K_POS * pos_err
    # vel_sp = array([8, 0, 0])
    vel_sp = constrain_ref(vel_sp, MAXI_VEL)
    vel_err = vel_sp - vel
    vel_sp_dot = (vel_sp - last_v_sp) / ts
    vel_sp_dot = constrain_ref(vel_sp_dot, MAXI_ACC)

    # SMC Controller
    sigma_pos = vel_err + Kc_P * pos_err
    s_pos = sigma_pos + pos_smdo_state[0:3]
    Log_s_pos.append(s_pos)
    Log_sigma_pos.append(sigma_pos)
    Log_V_sp.append(vel_sp)
    phi0_pos = Kc_P * vel_err - array([0, 0, 9.8]) + vel_sp_dot
    smdo_omega = lamda0_pos * sqrt(
        abs(s_pos)) * sign(s_pos) + pos_smdo_state[3:]
    u_eq = phi0_pos + K_SMC_POS * sigma_pos

    thrust_max_NE_tilt = abs(u_eq[2] * np.tan(45 / 57.3))
    lim_thr_max = 35
    thrust_max_NE = 0.0
    thrust_NE_remain_squre = lim_thr_max * lim_thr_max - u_eq[2] * u_eq[2]
    if thrust_NE_remain_squre > 0:
        thrust_max_NE = sqrt(thrust_NE_remain_squre)
    thr_max = thrust_max_NE_tilt
    if thrust_max_NE < thrust_max_NE_tilt:
        thr_max = thrust_max_NE

    thrust_sp_xy = u_eq[0:2]
    thrust_sp_xy_norm = np.linalg.norm(thrust_sp_xy)

    if thrust_sp_xy_norm > thr_max:
        u_eq[0] = u_eq[0] / thrust_sp_xy_norm * thr_max
        u_eq[1] = u_eq[1] / thrust_sp_xy_norm * thr_max

    u_eq = u_eq + smdo_omega

    T_c = -np.linalg.norm(u_eq) * mass
    acc_sp = vector3_normalize(u_eq)
    last_v_sp = vel_sp
    return T_c, acc_sp, s_pos, smdo_omega, phi0_pos


def generate_attitude_setpoint(acc_sp, yaw_sp, yaw):

    # desired body_z axis = -normalize(thrust_vector)
    body_z = -acc_sp

    # Vector of current yaw direction in XY plane, rotated by PI/2
    y_c = array([-sin(yaw), cos(yaw), 0.0])

    #  Desired body_x axis, orthogonal to body_z
    body_x = cross(y_c, body_z)
    body_x = vector3_normalize(body_x)

    # Desired body_y axis
    body_y = cross(body_z, body_x)
    body_y = vector3_normalize(body_y)

    # Fill rotation matrix
    att_dcm = zeros((3, 3))
    for i in range(0, 3):
        att_dcm[i, 0] = body_x[i]
        att_dcm[i, 1] = body_y[i]
        att_dcm[i, 2] = body_z[i]
    # Calculate Roll Pitch Setpoints
    dcm = Rotation.from_matrix(att_dcm)
    rpy_tmp = dcm.as_euler('zyx')
    att_sp = array([rpy_tmp[2], rpy_tmp[1], yaw_sp])
    att_sp[0] = constrain_ref(att_sp[0], 40 / 57.3)
    att_sp[1] = constrain_ref(att_sp[1], 40 / 57.3)
    return att_sp


Iv = np.array([[0.011, 0, 0], [0, 0.015, 0], [0, 0, 0.021]])
mass = 1.5
uav_b = sqrt(2) / 4 * 0.418
uav_l = sqrt(2) / 4 * 0.418
k1 = 1.3e-5
k2 = 4.7e-6
bk1 = uav_b * k1
lk1 = uav_l * k1
Gm = np.array([[-bk1, bk1, bk1, -bk1, bk1, -bk1, -bk1, bk1],
               [lk1, lk1, -lk1, -lk1, lk1, lk1, -lk1, -lk1],
               [-k2, k2, -k2, k2, -k2, k2, -k2, k2],
               [-k1, -k1, -k1, -k1, -k1, -k1, -k1, -k1]])
Gt = np.array([[0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0],
               [-k1, -k1, -k1, -k1, -k1, -k1, -k1, -k1]])
Gtm = (1 / mass) * Gt
Iv_inv = pinv(Iv)
a = np.zeros(3)
c = np.column_stack((Iv_inv, a))
b = np.array([[0, 0, 0, 1 / mass]])
tmp = np.row_stack((c, b))
Gu = dot(tmp, Gm)
Gm_inv = pinv(Gu)
g = 9.8
Fg = np.array([0, 0, mass * (g + 0.12)])
Cd = 1.8
S_im = 0.3 * 2

c = 1.5
k = [0.5, 0.5, 0.5, 1]
ks = [4, 5, 3, 8]
L = np.array([40, 40, 40])

lamda0 = 1.5 * sqrt(L)
lamda1 = 1.1 * L


def quad_copter_solo(t, x, u, v_wind):
    """ 6DOF EOM of QuadRotor
    This function defines the EOM of a quadrotor model.

    Param
    --------
    x : float
        states, state order:
        ------------------------
        0           1           2
        Position N, Position E, Position D,
        3           4           5
        Velocity N, Velocity E, Velocity D,
        6     7      8
        Roll, Pitch, Yaw,
        9  10 11
        p, q, r
        -------------------------
    u : int
        square of rotation speed of each motor.
    nu : float
    s : float
        sliding variable

    Return Value
    -------------------
    dxdt : float
           derivative of the states x.
    """
    velocity_ned = x[3:6]
    rpy = x[6:9]
    omega = x[9:12]
    IvOmega = Iv.dot(omega)
    Mv = -Iv_inv.dot((cross(omega, IvOmega)))
    Mu = Gu.dot(u)
    disturb = sin(2 * t) * np.ones(3)
    # disturb = zeros(3)
    omega_dot = Mv + Mu[0:3]
    dcm = Rotation.from_euler('zyx', [rpy[2], rpy[1], rpy[0]], degrees=False)
    Ft_body = Gt.dot(u)
    Ft_NED = dcm.as_matrix().dot(Ft_body)
    v_as = velocity_ned - v_wind
    v_square = v_as * v_as
    # aerodynamic drag along
    F_aero = -0.5 * 1.225 * v_square * Cd * S_im * sign(v_as)
    v_dot = (Ft_NED + Fg + F_aero) / (mass + 0.1)
    # z_dot = -phi_att + Mu[0:3] - nu
    # eta_dot = lamda1 * sign(s)
    dxdt = np.hstack((velocity_ned, v_dot, omega, omega_dot))
    return dxdt


widgets = [
    'Simulation Progress: ',
    pb.Percentage(), ' ',
    pb.AnimatedMarker(), ' ',
    pb.Timer(), ' ',
    pb.AdaptiveETA(), ' '
]
pbar = pb.ProgressBar(widgets=widgets, maxval=Te + ts).start()

sim_time = np.linspace(0, Te, int(Te / ts))

omega0 = ([0, 0, 0])
rpy0 = [0, 0, 0]
# height0 = 0
x0 = np.hstack((zeros(6), rpy0, omega0))

K = np.array([15, 15, 6])

k_att = np.array([20, 20, 20])

Log_err = []
Log_u = []
Log_s = []
Log_s_pos = []
Log_sigma = []
Log_sigma_pos = []
Log_V_sp = []
state = []
Log_Sp = []
Log_nu = []
pos_sp = array([0, 0, 0])
# print('Simulation is Running')
old_sp = 0
kc_att = array([0, 0, 0])
v_wind = array([-2, 0.2, 0])
v_wind = zeros(3)
max_rps = 1000
max_rps_square = max_rps * max_rps
omega_filtered = zeros(3)
eta_filtered = zeros(3)
u_old = zeros(4)
s_att = zeros(3)
smdo_att_omega = zeros(3)
phi0_att = zeros(3)
for t in sim_time:
    pos_ned = x0[0:3]
    vel_ned = x0[3:6]
    rpy = x0[6:9]
    omega = x0[9:12] + \
        array([gauss(-0.2, 0.2), gauss(-0.2, 0.2), gauss(-0.2, 0.2)])

    pos_sp = array([0.0, 0.0, -1.0])
    if t > 0:
        pos_sp = array([3, 1.5, -1.0])
    # T_c, thrust_sp = position_control(pos_sp, pos_ned, vel_ned)
    T_c, thrust_sp, s_pos, smdo_omega, phi0_pos = smc_smdo_position_control(
        pos_sp, pos_ned, vel_ned)
    Att_Sp = generate_attitude_setpoint(thrust_sp, 0, rpy[2])
    # Att_Sp = zeros(3)
    # Att_Sp[0] = gauss(-0.2,0.2)
    # if t > 1:
    #     Att_Sp = array([0.3, 0.0, 0.0])
    # if t > 1.5:
    #     Att_Sp = array([-0.3, 0.0, 0.0])
    # if t > 2:
    #     Att_Sp = array([0, 0.0, 0.0])
    # if t > 2.5:
    #     Att_Sp = array([-0.3, 0.0, 0.0])
    # if t > 8:
    #     Att_Sp = array([0.3, 0.0, 0.0])
    # if t > 10:
    #     Att_Sp =zeros(3)

    att_err = Att_Sp - rpy

    Omega_Sp = k_att * att_err
    # Omega_Sp = zeros(3)
    Omega_Sp = constrain_ref(Omega_Sp, array([3.5, 3.5, 3.5]))

    yc_dot = (Omega_Sp - old_sp) / ts
    # yc_dot[0] = cos(3*t)*0.3*8
    # yc_dot = constrain_ref(yc_dot,array([5,5,5]))
    # yc_dot = zeros(3)
    err = Omega_Sp - omega

    # Angular rate controller
    IvOmega = np.dot(Iv, omega)
    f = -Iv_inv.dot((cross(omega, IvOmega)))
    phi0_att = -f + kc_att * err + yc_dot
    sigma = err + kc_att * att_err

    s_att = sigma + att_smdo_state[0:3]

    # eta_filtered = eta_filtered*(0.5/(0.5+ts)) + att_smdo_state[3:]*(ts/(0.5+ts))

    smdo_att_omega = lamda0 * sqrt(abs(s_att)) * \
        sign(s_att) + att_smdo_state[3:]
    for i in range(0, 3):
        if abs(s_att[i]) < 0.5:
            smdo_att_omega[i] = 0.15 * lamda0[i] * \
                sqrt(abs(s_att[i])) * sign(s_att[i]) + att_smdo_state[3+i]
        if abs(smdo_att_omega[i]) < 5:
            omega_filtered = omega_filtered * \
                (12/(12+ts)) + smdo_att_omega*(ts/(12+ts))
        else:
            omega_filtered = omega_filtered * \
                (0.05/(0.05+ts)) + smdo_att_omega*(ts/(0.05+ts))
    # omega_filtered = smdo_att_omega;
    # omega_filtered = omega_filtered*(10/(10+ts)) + smdo_att_omega*(ts/(10+ts))
    u_eq = omega_filtered + phi0_att + K * sigma
    # u_eq = omega_filtered + phi0_att + K * sigma

    # u_eq[1] = omega_pitch_filtered + phi0_att[1] + K[1] * sigma[1]
    tmp_u = np.hstack((u_eq, T_c))

    u = Gm_inv.dot(tmp_u.transpose())
    u = constrain_saturation(u, 0, max_rps_square)

    Log_err.append(err)
    Log_u.append(u)
    Log_s.append(s_att)
    Log_sigma.append(sigma)
    state.append(x0)
    Log_Sp.append(np.hstack((pos_sp, Att_Sp, Omega_Sp)))
    Log_nu.append(omega_filtered)
    # assume lost 5% thrust when t = 10s
    pw_loss = 0.0 * t
    u_act = (1 - pw_loss) * u
    if t > 10:
        u_act[0] = 0.0
        u_act[1] = 0.0
    #     u_act = 0.6 * u_act
    # if t > 10:
    #     u_act[0] = 0.0
    #     u_act[7] = 0.0

    # v_wind = array([2, 2, 0])

    u_act = constrain_saturation(u_act, 0, max_rps_square)

    sol = integrate.solve_ivp(
        lambda t, x: quad_copter_solo(t, x, u_act, v_wind), [0, ts],
        x0,
        t_eval=[0, ts])
    x0 = sol.y[:, 1]

    # Att SMDO
    u_att = u  # no idea of fault
    sol = integrate.solve_ivp(lambda t, x: smdo_aux_att_dynamic(
        t, x, u_att, s_att, smdo_att_omega, phi0_att), [0, ts],
                              att_smdo_state,
                              t_eval=[0, ts])
    att_smdo_state = sol.y[:, 1]

    # u_old = u;
    # # Att SMDO
    # u_att = u  # no idea of fault
    # sol = integrate.solve_ivp(lambda t, x: smdo_aux_att_dynamic(
    #     t, x, u_att, s_att, smdo_att_omega, phi0_att), [0, ts],
    #     att_smdo_state,
    #     t_eval=[0, ts])
    # att_smdo_state = sol.y[:, 1]

    # # POS SMDO
    dcm = Rotation.from_euler('zyx', [rpy[2], rpy[1], rpy[0]], degrees=False)
    Ft_body = Gtm.dot(u)
    Ft_NED = dcm.as_matrix().dot(Ft_body)
    u_eq_pos = Ft_NED / mass
    sol = integrate.solve_ivp(lambda t, x: smdo_aux_pos_dynamic(
        t, x, u_eq_pos, s_pos, smdo_omega, phi0_pos), [0, ts],
                              pos_smdo_state,
                              t_eval=[0, ts])
    pos_smdo_state = sol.y[:, 1]

    pbar.update(t + ts)
    old_sp = Omega_Sp
pbar.finish()
# Save the results
# TODO: use a flag to determine whether to save the log file
state = np.array(state)
Omega_Sp = np.array(Log_Sp)
Log_err = np.array(Log_err)
Log_u = np.array(Log_u)
Log_s = np.array(Log_s)
Log_sigma = np.array(Log_sigma)
timestamp = np.reshape(sim_time, (-1, 1))
Log_Results = np.hstack(
    (timestamp, state, Log_Sp, Log_err, Log_u, Log_s, Log_sigma))
label_state = [
    'Position N', 'Position E', 'Position D', 'Velocity N', 'Velocity E',
    'Velocity D', 'Roll', 'Pitch', 'Yaw', 'p', 'q', 'r'
]
label_sp = [
    'PosNSp', 'PosESp', 'PosDSp', 'RollSp', 'PitchSp', 'YawSp', 'p_Sp', 'q_Sp',
    'r_Sp'
]
label_err = ['p_err', 'q_err', 'r_err']
label_u = ['u1^2', 'u2^2', 'u3^2', 'u4^2', 'u5^2', 'u6^2', 'u7^2', 'u8^2']
label_s = ['s1', 's2', 's3']
label_sigma = ['sigma1', 'sigma2', 'sigma3']
label = np.hstack(('timestamp', label_state, label_sp, label_err, label_u,
                   label_s, label_sigma))
dataframe = pd.DataFrame(data=Log_Results, columns=label)
log_file_time = time.strftime("%m%d-%H%M", time.localtime())
log_file_name = 'QuadSimLog' + log_file_time + '.csv'
# dataframe.to_csv(log_file_name, index=False, sep=',')
Log_Sp = array(Log_Sp)
Log_u = array(Log_u)

plt.figure()

plt.title('State X1')
plt.plot(sim_time, state[:, 0], color='red', label='N')
plt.plot(sim_time,
         Log_Sp[:, 0],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='N_Sp')
plt.plot(sim_time, state[:, 1], color='yellow', label='E')
plt.plot(sim_time, state[:, 2], color='blue', label='D')
plt.xlim((0, Te))
plt.legend(loc='upper right')
plt.grid(True)

# Log_s_pos = array(Log_s_pos)
# plt.figure()
# plt.plot(sim_time, Log_s_pos[:, 0], color='red', label='S_N')
# # plt.plot(sim_time, Log_s_pos[:, 1], color='yellow', label='S_E')
# # plt.plot(sim_time, Log_s_pos[:, 2], color='blue', label='S_D')
# plt.xlim((0, Te))
# plt.legend(loc='upper right')
# plt.grid(True)

Log_nu = array(Log_nu)
plt.figure()
plt.plot(sim_time, Log_nu[:, 0], color='red', label='Nu_roll')
# plt.plot(sim_time, Log_s_pos[:, 1], color='yellow', label='S_E')
# plt.plot(sim_time, Log_s_pos[:, 2], color='blue', label='S_D')
plt.xlim((0, Te))
plt.legend(loc='upper right')
plt.grid(True)

# Log_sigma_pos = array(Log_sigma_pos)
# plt.figure()
# plt.plot(sim_time, Log_sigma_pos[:, 0], color='red', label='Sigma_N')
# # plt.plot(sim_time, Log_sigma_pos[:, 1], color='yellow', label='S_E')
# # plt.plot(sim_time, Log_sigma_pos[:, 2], color='blue', label='S_D')
# plt.xlim((0, Te))
# plt.legend(loc='upper right')
# plt.grid(True)

# Log_V_sp = array(Log_V_sp)
# plt.figure()
# plt.plot(sim_time, state[:, 3], color='red', label='V_N')
# plt.plot(sim_time,
#          Log_V_sp[:, 0],
#          color='red',
#          linewidth=1.0,
#          linestyle='--',
#          label='VN_Sp')
# plt.plot(sim_time, state[:, 4], color='yellow', label='V_E')
# plt.plot(sim_time, state[:, 5], color='blue', label='V_D')
# # plt.plot(sim_time, Log_Sp[:, 0], label='Sp')
# plt.xlim((0, Te))
# plt.legend(loc='upper right')
# plt.grid(True)

plt.figure()
plt.plot(sim_time, state[:, 11], 'r', label='yaw rate')
plt.plot(sim_time, Log_Sp[:, 8], 'r-.', label='Yaw Rate Sp')
# plt.plot(sim_time, state[:, 10],'b', label='Pitch rate')
# plt.plot(sim_time, Log_Sp[:, 7], 'b-.', label='Pitch Rate Sp')
plt.xlim((0, Te))
plt.legend(loc='upper right')
plt.grid(True)

# plt.figure()
# plt.plot(sim_time, state[:, 9],'r', label='Roll Disturbance')
# plt.xlim((0, Te))
# plt.legend(loc='upper right')
# plt.grid(True)

plt.figure()
# plt.plot(sim_time, Log_sigma[:, 0],'r', label='Roll Sigma')
plt.plot(sim_time, Log_s[:, 0], 'b', label='Roll S')
plt.xlim((0, Te))
plt.legend(loc='upper right')
plt.grid(True)

plt.figure()
plt.plot(sim_time, state[:, 6] * 57.3, 'r', label='Roll')
plt.plot(sim_time, Log_Sp[:, 3] * 57.3, 'r-.', label='RollSp')
plt.plot(sim_time, state[:, 7] * 57.3, 'b', label='Pitch')
plt.plot(sim_time, Log_Sp[:, 4] * 57.3, 'b-.', label='PitchSp')
plt.plot(sim_time, state[:, 8] * 57.3, 'b', label='Yaw')
plt.plot(sim_time, Log_Sp[:, 5] * 57.3, 'b-.', label='YawSp')
plt.ylim((-20, 20))
plt.xlim((0, Te))
plt.legend(loc='upper right')
plt.grid(True)

plt.figure()
plt.plot(sim_time, sqrt(Log_u[:, 0]) * 60, label='motor1')
plt.plot(sim_time, sqrt(Log_u[:, 1]) * 60, label='motor2')
plt.plot(sim_time, sqrt(Log_u[:, 2]) * 60, label='motor3')
plt.plot(sim_time, sqrt(Log_u[:, 3]) * 60, label='motor4')
plt.xlim((0, Te))
plt.legend(loc='upper right')
plt.grid(True)
plt.show()
