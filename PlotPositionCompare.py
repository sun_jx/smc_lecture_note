import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

obj_P = pd.read_csv('QuadSimLog0209-1458.csv')
obj_SMDO = pd.read_csv('QuadSimLog0209-1459.csv')
ed = np.size(obj_SMDO[['timestamp']])
t_end = obj_SMDO.iloc[ed - 1]['timestamp']
t_end = 10

plt.figure()
plt.subplot(3, 1, 1)
plt.title('Position')
plt.plot(obj_SMDO[['timestamp']],
         obj_SMDO[['PosNSp']],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='Command')
plt.plot(obj_SMDO[['timestamp']], obj_SMDO[['Position N']], label='SMC/SMDO')
plt.plot(obj_P[['timestamp']], obj_P[['Position N']], label='PD Control')
plt.xlim((0, t_end))
plt.ylabel('N(m)')
plt.legend()
plt.grid(True)

plt.subplot(3, 1, 2)
plt.plot(obj_SMDO[['timestamp']],
         obj_SMDO[['PosESp']],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='Command')
plt.plot(obj_SMDO[['timestamp']], obj_SMDO[['Position E']], label='SMC/SMDO')
plt.plot(obj_P[['timestamp']], obj_P[['Position E']], label='PD Control')
# plt.legend(loc='upper right')
plt.legend()
plt.xlim((0, t_end))
plt.ylabel('E(m)')
plt.grid(True)

plt.subplot(3, 1, 3)
plt.plot(obj_SMDO[['timestamp']],
         obj_SMDO[['PosDSp']],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='Command')
plt.plot(obj_SMDO[['timestamp']], obj_SMDO[['Position D']], label='SMC/SMDO')
plt.plot(obj_P[['timestamp']], obj_P[['Position D']], label='PD Control')
plt.legend()
plt.xlim((0, t_end))
plt.ylim((-1.5, 1.5))
plt.ylabel('D(m)')
plt.grid(True)
plt.show()