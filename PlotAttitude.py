import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

obj_2 = pd.read_csv('QuadSimLog0207-1923.csv')
ed = np.size(obj_2[['timestamp']])
t_end = obj_2.iloc[ed - 1]['timestamp']

plt.figure()
plt.subplot(3, 1, 1)
plt.plot(obj_2[['timestamp']], obj_2[['Roll']], label='Roll')
plt.plot(obj_2[['timestamp']],
         obj_2[['RollSp']],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='Command')
plt.xlim((0, t_end))
plt.legend(loc='upper right')
plt.grid(True)

plt.subplot(3, 1, 2)
plt.plot(obj_2[['timestamp']], obj_2[['Pitch']], label='Pitch')
plt.plot(obj_2[['timestamp']],
         obj_2[['PitchSp']],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='Command')
plt.xlim((0, t_end))
plt.legend(loc='upper right')
plt.grid(True)

plt.subplot(3, 1, 3)
plt.plot(obj_2[['timestamp']], obj_2[['Yaw']], label='Yaw')
plt.plot(obj_2[['timestamp']],
         obj_2[['YawSp']],
         color='red',
         linewidth=1.0,
         linestyle='--',
         label='Command')
plt.xlim((0, t_end))
plt.legend(loc='upper right')
plt.grid(True)
plt.show()